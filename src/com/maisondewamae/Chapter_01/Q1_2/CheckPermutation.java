package com.maisondewamae.Chapter_01.Q1_2;

public class CheckPermutation {

    public static void main(String[] args) {
        System.out.println("Is permutation: " + check("benson", "nosneb"));
    }

    private static boolean check(String word1, String word2) {
        if (word1.length() != word2.length())
            return false;


        return sort(word1).equals(sort(word2));
    }

    private static String sort(String word) {
        char[] content = word.toCharArray();
        java.util.Arrays.sort(content);

        return new String(content);
    }

}
