package com.maisondewamae.Chapter_01.Q1_1;

public class IsUnique {

    public static void main(String[] args) {
	// Implement an algorithm to determine if a string has all unique characters
        //Ask interviewer if character are ASCII or UNICODE, if ASCII, extended ASCII has more characters(256)
        System.out.println("Is unique: "+isUnique("benson"));
    }

    private static boolean isUnique(String word){
        if(word.length() > 128) //If the word is longer than 128, reasons that some characters have been repeated
            return false;

        boolean[] chars = new boolean[128]; //

        for(int i= 0; i<word.length();i++){
            int myChar = word.charAt(i);
            if(chars[myChar]){
                return false;
            }

            chars[myChar] = true;
        }
        return true;
    }
}
