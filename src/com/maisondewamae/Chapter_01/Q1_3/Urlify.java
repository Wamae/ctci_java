package com.maisondewamae.Chapter_01.Q1_3;

public class Urlify {

    public static void main(String[] args) {
      String word = "Mr John Smith   ";
        int length = 13;
        String cleanWord = urlifyHelper(word,length);
        System.out.println(cleanWord.replace(" ","%20"));
    }

    private static String urlifyHelper(String word,int length) {

        char[] wordChar = word.toCharArray();
        return new String(java.util.Arrays.copyOfRange(wordChar,0,length));
    }


}
